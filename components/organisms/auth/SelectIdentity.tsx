import Image from 'next/image'

import * as Login from '../../molecules/Auth/Login'
import * as Identity from '../../molecules/Auth/Identity'
import avatarImage from '../../../public/images/guy.svg'
import playerTsImage from '../../../public/images/player_off.svg'
import chevronRightSvg from '../../../public/images/chevron-right-solid.svg'

const SelectIdentity = () => {
  return (
    <>
      <Login.Image>
        <Image
          width="150px"
          height="150px"
          src={avatarImage}
          alt='Authentication Image'
        />
      </Login.Image>
      <Login.Info>
        <div>Login</div>
        <div>Escolhe a tua Entidate TeamSpeak</div>

        {/* Identity Block - START */}
        <Identity.Container>
          <Identity.NameContainer>
            <Image
              width="25px"
              height="25px"
              src={playerTsImage}
              alt='Authentication Image'
            />
            Hard
          </Identity.NameContainer>
          <Identity.Status>Novo</Identity.Status>
            <Image
              width="25px"
              height="25px"
              src={chevronRightSvg}
              alt='Authentication Image'
            />
        </Identity.Container>
        {/* Identity Block - END */}

      </Login.Info>
    </>
  )
}

export default SelectIdentity