import Image from "next/image";

import * as Login from "../../molecules/Auth/Login";
import * as Identity from "../../molecules/Auth/Identity";
import avatarImage from "../../../public/images/guy.svg";
import playerTsImage from "../../../public/images/player_off.svg";
import chevronLeftSvg from "../../../public/images/chevron-left-solid.svg";
import * as Form from "../../../components/molecules/Form";
import { Button } from "../../atoms/Button";

const CreateAccount = () => {
  return (
    <>
      <Login.Image>
        <Image
          width="150px"
          height="150px"
          src={avatarImage}
          alt="Authentication Image"
        />
      </Login.Image>
      <Login.Info>
        <div>Criar Conta</div>
        <div>Precisamos de algumas informacoes</div>
        <Form.Container>
          <Identity.Container noBorder>
            <Identity.NameContainer>
              <Image
                width="25px"
                height="25px"
                src={chevronLeftSvg}
                alt="Authentication Image"
              />
              <Image
                width="25px"
                height="25px"
                src={playerTsImage}
                alt="Authentication Image"
              />
              Hard
            </Identity.NameContainer>
            <Identity.Status>Novo</Identity.Status>
          </Identity.Container>
          <label>
            Name
          </label>
          <input
            type="text"
            name="name"
            data-name="Name"
            placeholder=""
            id="name"
          />
          <label>
            Email Address
          </label>
          <input
            type="email"
            name="email"
            data-name="Email"
            placeholder=""
            id="email"
          />
          <div>Nao sera enviado nenhum email de confirmacao</div>
          <Button>Connectar</Button>
        </Form.Container>
      </Login.Info>
    </>
  );
};

export default CreateAccount;
