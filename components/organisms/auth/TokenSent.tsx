import Image from 'next/image'

import * as Login from '../../molecules/Auth/Login'
import * as Identity from '../../molecules/Auth/Identity'
import messageImage from '../../../public/images/message.svg'

const SelectIdentity = () => {
  return (
    <>
      <Login.Image>
        <Image
          width="150px"
          height="150px"
          src={messageImage}
          alt='Authentication Image'
        />
      </Login.Image>
      <Login.Info>
        <div>Token enviado!</div>
        <div>Verica as mensagens privadas no TeamSpeak</div>
      </Login.Info>
    </>
  )
}

export default SelectIdentity