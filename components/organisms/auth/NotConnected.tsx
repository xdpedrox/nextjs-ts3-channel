import Image from 'next/image'

import * as Login from '../../molecules/Auth/Login'
import { Button } from '../../atoms/Button'
import questionImage from '../../../public/images/question.svg'

const NotConnected = () => {
  return (
    <>
      <Login.Image>
        <Image
          width="150px"
          height="150px"
          src={questionImage}
          alt='Authentication Image'
        />
      </Login.Image>
      <Login.Info>
        <div>Utilizador nao encrontrado</div>
        <div>Para fazer login e necessario estares connectado ao nosso servidor TeamSpeak.</div>
        <Button>Connectar</Button>
      </Login.Info>
    </>
  )
}

export default NotConnected