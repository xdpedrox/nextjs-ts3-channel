import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-grow: 1;
  margin-top: 5px;
  padding-right: 20px;
  padding-left: 20px;
  padding-bottom: 20px;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 10px;
  background-color: #fff;
`
