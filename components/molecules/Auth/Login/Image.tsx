import styled from 'styled-components'

export const Image = styled.div`
  height: 150px;
  margin: 40px 20px 30px;
`
