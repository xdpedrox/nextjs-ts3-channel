import styled from 'styled-components'

export const Layout = styled.div`
  display: flex;
  width: 100%;
  max-width: 1100px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`