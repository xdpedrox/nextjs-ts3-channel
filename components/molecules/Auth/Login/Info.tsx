import styled from 'styled-components'

export const Info = styled.div`
  display: flex;
  width: 100%;
  padding-right: 20px;
  padding-bottom: 20px;
  padding-left: 20px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
