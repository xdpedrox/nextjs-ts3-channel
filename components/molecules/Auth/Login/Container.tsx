import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  width: 90%;
  max-height: 700px;
  max-width: 500px;
  margin: 40px 20px;
  padding: 20px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 10px;
  background-color: #f6f6f6;
`
