import styled from "styled-components";
interface ContainerProps {
  noBorder?: any;
}

export const Container = styled.div<ContainerProps>`
  display: flex;
  width: 90%;
  height: 45px;
  margin-top: 5px;
  margin-bottom: 5px;
  padding-right: 20px;
  padding-left: 20px;
  justify-content: space-between;
  align-items: center;

  ${({ noBorder }) => !noBorder && `
    border-radius: 10px;
    background-color: #fff;
    box-shadow: 1px 1px 3px -1px #000;  
    `}
  `

