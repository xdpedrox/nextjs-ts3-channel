import styled from "styled-components";

export const Section = styled.div`
  position: fixed;
  left: 0%;
  top: 0%;
  right: 0%;
  bottom: auto;
  width: 100%;
  background-color: ${props => props.theme.colors.white};
  z-index: 10;
  border-bottom: solid 2px black;
`