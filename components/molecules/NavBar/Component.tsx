
import { Container } from './Container'
import { Navigation } from './Navigation'
import { NavLink } from './NavLink'
import { ButtonLink } from '../../atoms/Button'
import { Section } from './Section'

export default function NavBar() {
  return (
    <Section>
      <Container>
        <div>
          Elite <br /> Portuguesa
        </div>
        <Navigation>
          <NavLink href='#' name='Home' />
          {/* <NavLink href='#' name='Informacao' />
        <NavLink href='#' name='Statisticas' />
        <NavLink href='#' name='Sobre nos' />
        <NavLink href='#' name='Feedback' /> */}
        </Navigation>
        <ButtonLink l href="/criar-canal">Criar canal</ButtonLink>
      </Container>
    </Section>
  )
}