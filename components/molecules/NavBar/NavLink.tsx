import styled from "styled-components";
import Link from 'next/link'

interface NavLinkInterface {
  current?: any
}

const NavEntry = styled.a<NavLinkInterface>`
  padding: 17px 20px;
  color: ${props => props.theme.colors.purple};
  font-size: 20px;
  font-weight: 400;
  text-decoration: none;

  ${({ current }) => current && `
    font-weight: 500;
  `}
`


export function NavLink({ href, name }) {
  // Must add passHref to Link
  return (
    <Link href={href} passHref>
      <NavEntry>{name}</NavEntry>
    </Link>
  )
}

