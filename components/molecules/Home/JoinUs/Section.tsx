
import styled from 'styled-components'

export const Section = styled.div`
  display: flex;
  width: 100%;
  padding-top: 10px;
  padding-bottom: 100px;
  flex-direction: column;
  justify-content: center;
  flex-wrap: nowrap;
  align-items: center;
  `