import styled from 'styled-components'

export const Section = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 1100px;
  align-items: center;
`
