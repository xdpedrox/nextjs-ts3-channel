import styled from 'styled-components'

export const Container = styled.div`
  display: grid;
  width: 100%;
  grid-auto-columns: 1fr;
  grid-column-gap: 16px;
  grid-row-gap: 16px;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: auto auto;
  margin-top: 25px;
  margin-bottom: 20px;
  max-width: 1100px;

`


