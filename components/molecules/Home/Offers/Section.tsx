import styled from 'styled-components'

export const Section = styled.div`
  display: flex;
  width: 100%;
  padding-right: 20px;
  padding-left: 20px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #ffd464;
  background-position: 0px 0px;
  background-size: auto;
`


