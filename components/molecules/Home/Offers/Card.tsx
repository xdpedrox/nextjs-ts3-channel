import styled from 'styled-components'

export const Card = styled.div`
  display: flex;
  padding: 20px;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border: 1px none #000;
  border-radius: 20px;
  background-color: #f1f1f1;
  color: #000;
`
