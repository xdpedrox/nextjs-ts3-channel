import { Container } from "./Container";
import { FooterBox } from "./FooterBox";
import * as Text from "../../atoms/Text";
export default function Footer() {
  return (
    <Container>
      <FooterBox>
        <Text.Div
          fontSize={18}
          lineHeight={25}
          fontWeight={600}
          fontFamily="Montserrat"
        >
          Elite
          <br />
          Portuguesa
        </Text.Div>
        <Text.Div
          fontSize={18}
          lineHeight={25}
          fontWeight={600}
          fontFamily="Montserrat"
        >
          Designed and Created <br />
          by Pedro Roque
        </Text.Div>
        <Text.Div
          fontSize={18}
          lineHeight={25}
          fontWeight={600}
          fontFamily="Montserrat"
        >
          Copyright ©<br /> 2012 - 2021
        </Text.Div>
      </FooterBox>
    </Container>
  );
}
