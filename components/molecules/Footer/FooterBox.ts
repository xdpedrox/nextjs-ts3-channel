import styled from 'styled-components'

export const FooterBox = styled.div`
  display: flex;
  width: 100%;
  max-width: 1100px;
  margin-right: auto;
  margin-left: auto;
  padding-right: 35px;
  padding-left: 35px;
  justify-content: space-between;
  align-items: center;
  background-color: transparent;
`