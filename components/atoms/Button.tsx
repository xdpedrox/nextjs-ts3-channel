import styled from 'styled-components'
import Link from 'next/link'

interface ButtonProps {
  white?: any;
  s?: any;
  m?: any;
  l?: any;
  width?: number;
  height?: number;
}

export const Button = styled.button<ButtonProps>`
  border-radius: 8px;
  font-size: 17px;
  border: none;

  background-color: #fe6e70;
  color: #fff;

  padding-top: 5px;
  padding-bottom: 5px;
  padding-left: 10px;
  padding-right: 10px;

  ${({ white, theme }) => white && `
    background-color: ${theme.colors.white};
    color: ${theme.colors.red};
    border: 2px solid ${theme.colors.red};
  `}

  ${({ s }) => s && `
    height: 30px;
    font-size: 14px;
    width: 100px;
  `}

  ${({ l }) => l && `
    height: 50px;
    font-size: 18px;
    width: 200px;
  `}
  
  ${({ width }) => width && `
    width: ${width}px;
  `}

  ${({ height }) => height && `
    height: ${height}px;
  `}
`

export function ButtonLink(props) {
  // Must add passHref to Link
  return (
    <Link href={props.href} passHref>
      <a>
        <Button
          {...props}
        >{props.children}</Button>
      </a>
    </Link>
  )
}
