

import styled from 'styled-components'

const PageContent = styled.div`
  display: flex;
  flex: 1 0 auto;
  width: 100%;
  margin-top: 50px;
  padding-top: 20px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: auto;
  overflow: hidden;
`

export default PageContent
