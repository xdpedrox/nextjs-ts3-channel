import styled from "styled-components";


const fontSize = (props) => props.fontSize && `
  font-size: ${props.fontSize}px;
  line-height: ${props.fontSize + 10}px;
`

const lineHeight = (props) => props.lineHeight && `
  line-height: ${props.lineHeight}px;
`

const fontWeight = (props) => props.fontWeight && `
  font-weight: ${props.fontWeight};
`

const fontFamily = (props) => props.fontFamily && `
  font-family: '${props.fontFamily}', sans-serif;
`

const textAlign = (props) => props.textAlign && `
  text-align: ${props.textAlign};
`

const textColor = (props) => props.textColor && `
  color: ${props.theme.colors[props.textColor]};
`


interface TextProps {
  fontFamily?: string;
  fontWeight?: number;
  lineHeight?: number;
  fontSize?: number;
  textAlign?: string;
  textColor?: string;

}
export const Span = styled.div<TextProps>`
  margin: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
  text-align: center;
  ${textColor}
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}
`

export const Div = styled.div<TextProps>`
  margin: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
  text-align: center;
  ${textColor}
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}
`

export const H1 = styled.h1<TextProps>`
  margin: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
  text-align: center;
  ${textColor}
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}

`

export const H2 = styled.h2<TextProps>`
  margin: 0px;
  margin-top: 5px;
  margin-bottom: 10px;
  text-align: center;
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}
  ${textColor}

`

export const H3 = styled.h3<TextProps>`
  margin: 0px;
  margin-top: 5px;
  margin-bottom: 5px;
  text-align: center;
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textColor}

`

export const H4 = styled.h4<TextProps>`
  margin: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
  text-align: center;
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}
  ${textColor}

`

export const P = styled.p<TextProps>`
  margin: 0px;
  margin-top: 0px;
  margin-bottom: 0px;
  text-align: center;
  ${fontSize}
  ${lineHeight}
  ${fontWeight}
  ${fontFamily}
  ${textAlign}
  ${textColor}

`

