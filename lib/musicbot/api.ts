import { apiRequest } from './axios'
import { config } from './config'

// returns nothing
export const createMusicBot = async botName => {
  try {
    const origin = encodeURIComponent(config.templateName)
    const destination = encodeURIComponent(botName)

    return await apiRequest({
      url: `/api/settings/copy/${origin}/${destination}`
    })
  } catch (err) {
    console.log(err)
  }
}

// Return instance info
export const botConnect = async botName => {
  try {
    const encodedBotName = encodeURIComponent(botName)

    return await apiRequest({
      url: `/api/bot/connect/template/${encodedBotName}`
    })
  } catch (err) {
    console.log(err)
  }
}

// Return instance info
export const getInstanceInfo = async botName => {
  try {
    const encodedBotName = encodeURIComponent(botName)

    return await apiRequest({ url: `/api/bot/info/template/${encodedBotName}` })
  } catch (err) {
    console.log(err)
  }
}

// gets info about bot client on teamspeak
export const getBotClientInfo = async botName => {
  try {
    const encodedBotName = encodeURIComponent(botName)

    return await apiRequest({
      url: `/api/bot/template/${encodedBotName}/(/bot/info/client)`
    })
  } catch (err) {
    console.log(err)
  }
}

export const botDisconnect = async botName => {
  try {
    const encodedBotName = encodeURIComponent(botName)

    return await apiRequest({
      url: `/api/bot/template/${encodedBotName}/(/bot/disconnect/)`
    })
  } catch (err) {
    console.log(err)
  }
}

export const botSetNickname = async (botName, nickname) => {
  try {
    const encodedBotName = encodeURIComponent(botName)
    const encodedNickname = encodeURIComponent(nickname)

    return await apiRequest({
      url: `/api/settings/bot/set/${encodedBotName}/connect.name/${encodedNickname}`
    })
  } catch (err) {
    console.log(err)
  }
}

export const botSetDefaultChannel = async (botName, cid) => {
  try {
    const encodedBotName = encodeURIComponent(botName)
    const encodedChannel = encodeURIComponent(`/${cid}`)

    return await apiRequest({
      url: `/api/settings/bot/set/${encodedBotName}/connect.channel/${encodedChannel}`
    })
  } catch (err) {
    console.log(err)
  }
}

export const botRightsReload = async () => {
  try {
    return await apiRequest({ url: `/api/rights/reload` })
  } catch (err) {
    console.log(err)
  }
}

export const botSettingsReload = async botName => {
  try {
    const encodedBotName = encodeURIComponent(botName)
    
    return await apiRequest({
      url: `/api/settings/bot/reload/${encodedBotName}`
    })
  } catch (err) {
    console.log(err)
  }
}

// http://91.121.0.75:58913/api/bot/use/0/(/bot/info/client)
