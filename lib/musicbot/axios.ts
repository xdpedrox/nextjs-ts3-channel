import axios from "axios";

export const apiRequest = ({
  baseURL = process.env.MUSICBOT_URL,
  params = null,
  url = "",
  method = "GET",
  data = null,
}) => {
  // axios default configs
  axios.defaults.baseURL = baseURL;
  axios.defaults.headers.common["Authorization"] = process.env.MUSICBOT_TOKEN;

  return axios.request({
    url: url,
    // @ts-ignore
    method: method,
    data,
    params,
  });
};
