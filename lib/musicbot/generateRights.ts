import fs from 'fs'
const exportPath = './tests.toml'
const adminUuids = ['smgDpLgFrZmV2FrK/PwWvFCA2Pw=']
const adminServerGroups = [42]

const generateUuidArrayString = (uuids: string[]) => {
  return uuids.length > 0 ? `["${uuids.join(", ")}"]` : '[]'
}
const generateServerGroupArrayString = (serverGroups: number[]) => {
  return `[${serverGroups.join(', ')}]`
}

const base = `
# Admin rule
[[rule]]
	# Set your admin Group Ids here, ex: [ 13, 42 ]
	groupid = ${generateServerGroupArrayString(adminServerGroups)}
	# And/Or your admin Client Uids here
	useruid = ${generateUuidArrayString(adminUuids)}

	"+" = "*"

["$botUser"]
  "+" = [
    # Play controls
    "cmd.play",
    "cmd.pause",
    "cmd.stop",
    "cmd.seek",
    "cmd.volume",

    # Playlist management
    "cmd.list.*",
    "cmd.add",
    "cmd.clear",
    "cmd.previous",
    "cmd.next",
    "cmd.random.*",
    "cmd.repeat.*",

    # History features
    "cmd.history.add",
    "cmd.history.from",
    "cmd.history.id",
    "cmd.history.last",
    "cmd.history.play",
    "cmd.history.till",
    "cmd.history.title",
  ]

# =======
# Bot Instances
# =======

`

const generateEntry = ({ name, serverGroups, uuids }: botInstancesEntry) => {
  return `
# ${name}
[[rule]]
  bot = [ "${name}" ]
  [[rule.rule]]
    groupid = ${generateServerGroupArrayString(serverGroups)}
    useruid = ${generateUuidArrayString(uuids)}
    include = [ "$botUser" ]
`
}

interface botInstancesEntry {
  name: string
  uuids: string[]
  serverGroups: number[]
}

type botInstance = botInstancesEntry[]



 const generateRights = async (instances: botInstance) => {
  let file = base
  instances.forEach(entry => {
    file += generateEntry(entry)
  })
  await writeFile(file)
}

const writeFile = async content => {
  try {
    await fs.writeFileSync(exportPath, content)
  } catch (err) {
    console.error(err)
  }
}

export default generateRights
generateRights([
  { name: 'bot4', uuids: ['HwJnezBqlcLPvFej8M1/R9qXC5g='], serverGroups: [42] },
])
