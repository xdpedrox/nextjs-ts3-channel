import { ts3 } from "../../../lib/ts3/src/index";
import { randomBytes } from "crypto";
import addHours from "date-fns/addHours";

import 'reflect-metadata';
import getDb from "../../../config/initialize-database";

async function handler(req: any, res: any) {
  const db = await getDb();

  if (req.method !== "POST") {
    return;
  }

  const data = req.body;

  if (!data.uuid) {
    res.status(400).json({
      message: "Please provide uuid",
    });
    return;
  }

  const clients = await ts3.clientList({ clientUniqueIdentifier: data.uuid });

  if (clients.length == 0) {
    res.status(200).json({
      message: "Client not connected",
    });
    return;
  }


  // // Find of Create User
  let user = await db.userRepo.findOne({ uuid: data.uuid });

  if (!user) {
    if (data.email && data.name) {
     user = await db.userRepo.create({
        uuid: data.uuid,
        email: data.email,
        name: data.name,
      });
      await db.userRepo.persist(user).flush();
      console.log("User created");
    } else {
      res.status(403).json({
        message: "User not found, please provide a name and email",
      });
      return;
    }
  }

  const expireDate = addHours(new Date(), 2);

  const verificationRequest = await db.verificationRequestRepo.create({
    identifier: user.uuid,
    token: randomBytes(32).toString("hex"),
    expires: expireDate,
  });
  await db.verificationRequestRepo.persist(verificationRequest).flush();


  // send message to all clients
  const messageArrays = clients.map((client) => {
    const url = `${req.headers.origin}/auth/callback?uuid=${encodeURIComponent(
      verificationRequest.identifier
    )}&token=${encodeURIComponent(verificationRequest.token)}`;
    client.message(url);
  });

  await Promise.all(messageArrays);
}

export default handler;
