
// https://next-auth.js.org/configuration/callbacks
import 'reflect-metadata';
import getDb from "../../../config/initialize-database";

// @ts-nocheck
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

export default NextAuth({
  providers: [
    Providers.Credentials({
      async authorize(credentials: {uuid: string, token: string}) {
        const db = await getDb();

        if (!credentials.uuid && !credentials.token) {
          throw new Error('Invalid Input');
        }

        const verificationRequest = await db.verificationRequestRepo.findOne({ identifier: credentials.uuid, token: credentials.token})

        if (!verificationRequest) {
          throw new Error('Invalid Token');
        }

        const user = await db.userRepo.findOne({ uuid: verificationRequest.identifier})

        if (!user) {
          throw new Error('User not found');
        }

        await db.verificationRequestRepo.remove(verificationRequest).flush()
        
        return { uuid: user.uuid };
      },
    }),     
  ],
  
  secret: process.env.SECRET,

  session: {
    jwt: true,
    maxAge: 30 * 24 * 60 * 60, // 30 days
  },

  jwt: {
    secret:  process.env.JWT_SECRET, //use a random secret token here
    encryption: true,
  },

  callbacks: {
    async session(session, token) {
      // @ts-ignore
      session.user = token.user;
      return session;
    },
    async jwt(token, user, account, profile, isNewUser) {
      if (user) {
        token.user = user;
      }
      return token;
    },
  },

  pages: {
    signIn: "/login",
    signOut: "/",
  },
  
  debug: true,
})
