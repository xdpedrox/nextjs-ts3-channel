// Create Team

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import permitParams from "permit-params";
import createTeam from "../../services/createTeam";
import { getSession } from "next-auth/client";
import getDb from "../../config/initialize-database";
import { apiHandler } from "../../helpers/api-handler";
import isAuthenticated from "../../middleware/isAuthenticated";
import doesNotHaveTeam from "../../middleware/doesNotHaveTeam";

export default apiHandler({
  post: createTeamRequest,
});

const uuid = "smgDpLgFrZmV2FrK/PwWvFCA2Pw=";

interface CreateTeamParams {
  team_name: string;
  password: string;
  move: boolean;
}

async function createTeamRequest(req: NextApiRequest, res: NextApiResponse) {
  const session = await getSession({ req: req });
  await isAuthenticated(session)

  const db = await getDb()
  const user = await db.userRepo.findOne({ uuid: uuid }, ['team'])

  await doesNotHaveTeam(user)

  // Useless where should check the type of each content 
  // to make sure they meet the requirements
  const response = await createTeam(
    uuid,
    req.body.team_name,
    req.body.password,
    req.body.move
  );
  res.status(response.statusCode).json(response.body);
}
