// View SubChannels
// Create SubChannel
// Delete SubChannel

import { NextApiRequest, NextApiResponse } from "next";
import getDb from "../../../../config/initialize-database";
import { apiHandler } from "../../../../helpers/api-handler";


export default apiHandler({
  get: getSubChannels,
  put: createSubChannel,
  delete: deleteSubChannel,
});


async function getSubChannels (req: NextApiRequest, res: NextApiResponse) {};

async function createSubChannel (req: NextApiRequest, res: NextApiResponse) {};

async function deleteSubChannel (req: NextApiRequest, res: NextApiResponse) {};
