// View user's team
// Change Name
// Delete Team

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from "next";
import permitParams from "permit-params";
import changeTeamName from "../../../../services/changeTeamName";
import { getSession } from "next-auth/client";
import getDb from "../../../../config/initialize-database";
import { apiHandler } from "../../../../helpers/api-handler";
import hasTeam from "../../../../middleware/hasTeam";
import isAuthenticated from "../../../../middleware/isAuthenticated";
import isChannelAdmin from "../../../../middleware/isChannelAdmin";

const uuid = "smgDpLgFrZmV2FrK/PwWvFCA2Pw=";

export default apiHandler({
  get: getUserTeam,
  delete: deleteTeam,
  patch: editTeam,
});

// Get user's team
async function getUserTeam(req: NextApiRequest, res: NextApiResponse) {
  const session = await getSession({ req: req });
  await isAuthenticated(session)

  const db = await getDb();
  const user = await db.userRepo.findOne({ uuid: uuid }, ["team"]);

  await hasTeam(user)

  res.status(200).json(user.team);
}


// Change Team Name
async function editTeam(req: NextApiRequest, res: NextApiResponse) {
  if (!req.body.name) {
    throw 'Invalid input'
  }

  const session = await getSession({ req: req });
  await isAuthenticated(session)

  const db = await getDb()
  const user = await db.userRepo.findOne({ uuid: uuid }, ['team'])
  
  await isChannelAdmin(user)
  await hasTeam(user)

  await changeTeamName(user.team, req.body.name);

  res.status(200).json(user.team);
}

// Delete Team
async function deleteTeam (req: NextApiRequest, res: NextApiResponse) {};
