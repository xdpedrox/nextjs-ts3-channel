// Create Server Group
import 'reflect-metadata'
import getDb from '../../../../config/initialize-database'
import isChannelAdmin from '../../../../middleware/isChannelAdmin'
import isAuthenticated from '../../../../middleware/isAuthenticated'
import { apiHandler } from '../../../../helpers/api-handler'
import { getSession } from 'next-auth/client'
import createServerGroup from '../../../../services/createServerGroup'


const uuid = 'smgDpLgFrZmV2FrK/PwWvFCA2Pw='

export default apiHandler({
  post: createGroup,
});

// Creates serverGroup for the user's team
async function createGroup(req: any, res: any) {
  try {
    const session = await getSession({ req: req });
    await isAuthenticated(session)

    const db = await getDb()
    const user = await db.userRepo.findOne({ uuid: uuid }, ['team'])
    
    await isChannelAdmin(user)

    await createServerGroup(user.team)

    res.json({ message: 'User is Admin' })
  } catch (err) {
    res.status(500).json(err)
  }
}

