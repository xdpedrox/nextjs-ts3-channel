import { NextApiRequest, NextApiResponse } from "next";
import getDb from "../../../../config/initialize-database";
import { apiHandler } from "../../../../helpers/api-handler";

// To manage users of a team. 
// GET - List Users 
// PUT - Set user permission, 
// DELETE - Remove Users


export default apiHandler({
  get: getTeamUsers,
  put: setUserPermission,
  delete: removeUser,
});


async function getTeamUsers (req: NextApiRequest, res: NextApiResponse) {};

async function setUserPermission (req: NextApiRequest, res: NextApiResponse) {};

async function removeUser (req: NextApiRequest, res: NextApiResponse) {};
