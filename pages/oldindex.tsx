// @ts-nocheck

import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useSession, signOut } from 'next-auth/client';

export default function Home () {
  const [session, loading] = useSession()
  console.log(session)
  return (
    <div className={styles.container}>
      {session && (
        <p>
          {session.user.uuid}{' '}
          <button
            className='underline'
            onClick={() => {
              signOut()
              router.push('/')
            }}
          >
            logout
          </button>
        </p>
      )}
    </div>
  )
}
