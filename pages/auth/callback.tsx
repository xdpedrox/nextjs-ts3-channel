// src/pages/auth/signin.tsx

import { useRouter } from "next/router";
import { signIn } from "next-auth/client";
import { useEffect } from "react";

export default function SignIn() {
  const router = useRouter();

  const { uuid, token } = router.query;

  if (uuid && token) {
    signIn("credentials", {
      redirect: true,
      uuid: uuid,
      token: token,
      callbackUrl: "http://localhost:3000/",
    }).then((r) => {
      console.log("Login Successfull");
    }).catch((err) => {
      console.log(err);
      console.log("shit is on fire yo");
    });
  }

  return <div>Signin Page :)</div>;
}
