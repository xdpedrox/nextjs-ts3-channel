import '../styles/normalize.css'
import '../styles/styles.css'

import type { AppProps } from 'next/app'
import Script from "next/script";

import { ThemeProvider } from 'styled-components'
import GlobalStyle from '../components/GlobalStyles'


const theme = {
  colors: {
    white: '#fff',
    black: '#000',
    purple: '#36245c',
    red: '#fe6e70',
    yellow: '#ffd464',
    brown: '#734'
  },
  channelOwnerColors: {
    dono: "#e94646",
    admin: "#ffca19",
    mod: "silver",
    member: "#cd7f32"
  },
  fontSizes: {
    xl: 60,
    l: 35,
    m: 30,
    s: 25,
    text: 22
  },
}

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>

      <Script
        strategy="lazyOnload"
        src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
      />
      <Script strategy="lazyOnload">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}');
        `}
      </Script>


      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  )
}