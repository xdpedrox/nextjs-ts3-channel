// @ts-nocheck

import Image from "next/image";
import Footer from "../components/molecules/Footer";
import AppContainer from "../components/atoms/AppContainer";
import NavBar from "../components/molecules/NavBar";
import PageContent from "../components/atoms/PageContent";
import { Button, ButtonLink }  from "../components/atoms/Button";
import gamingImage from "../public/images/gaming.svg";

import * as Intro from "../components/molecules/Home/Intro";
import * as Stats from "../components/molecules/Home/Stats";
import * as Offers from "../components/molecules/Home/Offers";
import * as JoinUs from "../components/molecules/Home/JoinUs";

// Images
import clockImage from "../public/images/clock.svg";
import infinityImage from "../public/images/infinity.svg";
import headphonesImage from "../public/images/headphones.svg";
import groupImage from "../public/images/home/group.png";
import gamesImage from "../public/images/home/games.png";
import channelImage from "../public/images/home/channel.png";
import * as Text from "../components/atoms/Text";

export default function Home() {
  return (
    <AppContainer>
      <NavBar />
      <PageContent>
        <Intro.Section>
          <Intro.Container>
            <Text.Span
              fontSize={70}
              fontWeight={400}
              fontFamily="Sourceserifpro"
              textAlign="left"
              textColor="purple"
            >
              Junta-te ao melhor
            </Text.Span>
            <Text.Span
              fontSize={70}
              fontWeight={700}
              fontFamily="Sourceserifpro"
              textAlign="left"
              textColor="purple"
            >
              TeamSpeak de Portugal
            </Text.Span>

            <Intro.Buttons>
              <ButtonLink l href="/criar-canal">Criar canal</ButtonLink>

              <ButtonLink l white href="ts3server://elitept">Entra no TeamSpeak</ButtonLink>
            </Intro.Buttons>
          </Intro.Container>
          <div>
            <Image
              layout="fixed"
              height="490px"
              width="490px"
              src={gamingImage}
              alt="Authentication Image"
            />
          </div>
        </Intro.Section>

        <Stats.Section>
          <Text.H1
            fontSize={50}
            lineHeight={70}
            fontWeight={700}
            fontFamily="Sourceserifpro"
            textColor="purple"
          >
            Centenas de jogadores escolhem a <br />
            Elite Portuguesa
          </Text.H1>
          <Stats.Container>
            <Stats.Entry>
              <Text.Div
                fontSize={60}
                lineHeight={60}
                fontWeight={400}
                fontFamily="Merriweather"
                textColor="brown"
              >
                317
              </Text.Div>
              <Text.Div
                fontSize={18}
                lineHeight={25}
                fontWeight={400}
                fontFamily="Nunito"
                textColor="purple"
              >
                Utilizadores Diarios
              </Text.Div>
            </Stats.Entry>

            <Stats.Entry>
              <Text.Div
                fontSize={60}
                lineHeight={60}
                fontWeight={400}
                textColor="brown"
                fontFamily="Merriweather"
              >
                20668
              </Text.Div>
              <Text.Div
                fontSize={18}
                lineHeight={25}
                fontWeight={400}
                fontFamily="Nunito"
                textColor="purple"
              >
                Utilizadores
              </Text.Div>
            </Stats.Entry>

            <Stats.Entry>
              <Text.Div
                textColor="brown"
                fontSize={60}
                lineHeight={60}
                fontWeight={400}
                fontFamily="Merriweather"
              >
                124
              </Text.Div>
              <Text.Div
                fontSize={18}
                lineHeight={25}
                fontWeight={400}
                fontFamily="Nunito"
                textColor="purple"
              >
                Canais Permanetes
              </Text.Div>
            </Stats.Entry>
          </Stats.Container>
        </Stats.Section>

        <img
          src="/images/section-top.svg"
          loading="lazy"
          alt=""
          className="section-image-top"
        />
        <Offers.Section>
          <Text.H1
            textColor="purple"
            fontSize={50}
            lineHeight={50}
            fontWeight={700}
            fontFamily="Sourceserifpro"
          >
            O que oferecemos
          </Text.H1>
          <Offers.Container>
            <Offers.Card>
              {/* Image */}
              <Image
                width="150px"
                height="150px"
                src={clockImage}
                alt="Authentication Image"
              />
              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Staff Activa
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Ha sempre um de nos que esta no teamspeak, estamos a uma
                mensagem de distancia para responder a qualquer questao.
              </Text.P>
            </Offers.Card>

            <Offers.Card>
              {/* Image */}
              <Image
                className="offer-image"
                width="180px"
                height="150px"
                src={infinityImage}
                alt="Authentication Image"
              />

              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Salas Permanentes
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Podes criar a tua sala permanente na no nosso website. Basta
                acederes ao Painel das Salas
              </Text.P>
            </Offers.Card>

            <Offers.Card>
              {/* Image */}
              <Image
                width="150px"
                height="150px"
                src={headphonesImage}
                alt="Authentication Image"
              />

              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Music Bot Gratis
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Toca todas as tuas playlist do youtube e soundcloud com o teu
                bot privado.
                <br />
                Disponivel para canais com mais de 1 mes.
              </Text.P>
            </Offers.Card>

            <Offers.Card>
              {/* Image */}
              <Image
                width="230px"
                height="150px"
                src={gamesImage}
                alt="Authentication Image"
              />

              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Adiciona Icons
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Adiciona os icons de jogos ou rank atravez do nosso website
              </Text.P>
            </Offers.Card>

            <Offers.Card>
              {/* Image */}
              <Image
                width="174px"
                height="150px"
                src={channelImage}
                alt="Authentication Image"
              />

              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Simples e Organizado
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Salas numeradas e limite de 6 icons por pessoa. Sem spam nem
                bots chatos que te movem
              </Text.P>
            </Offers.Card>

            <Offers.Card>
              {/* Image */}
              <Image
                width="300px"
                height="150px"
                src={groupImage}
                alt="Authentication Image"
              />

              <Text.H2
                fontSize={26}
                lineHeight={32}
                fontWeight={700}
                fontFamily="Nunito"
              >
                Grupo para o teu Canal
              </Text.H2>
              <Text.P
                fontSize={18}
                lineHeight={22}
                fontWeight={400}
                fontFamily="Nunito"
              >
                Se reconhecido pelo TS Adiciona o teu icon do teu grupo de
                amigos.
              </Text.P>
            </Offers.Card>
          </Offers.Container>
        </Offers.Section>
        <img
          src="/images/section-bot.svg"
          loading="lazy"
          alt=""
          className="section-image-bot"
        />

        <JoinUs.Section>
          <Text.H1
            fontSize={60}
            fontWeight={700}
            fontFamily="Sourceserifpro"
            textColor="purple"
          >
            Estas a espera do que?
          </Text.H1>
          <JoinUs.ButtonContainer>
          <ButtonLink l href="/criar-canal">Junta-te as nos</ButtonLink>
          </JoinUs.ButtonContainer>
        </JoinUs.Section>
      </PageContent>
      <Footer />
    </AppContainer>
  );
}
