// @ts-nocheck

import Head from 'next/head'
import { useRef, useState } from 'react'
import axios from 'axios'
import Image from 'next/image'

import AppContainer from '../components/atoms/AppContainer'
import Footer from '../components/molecules/Footer'
import NavBar from '../components/molecules/NavBar'
import * as Login from '../components/molecules/Auth/Login'
import { ButtonLink } from '../components/atoms/Button'
import PageContent from '../components/atoms/PageContent'
import authImage from '../public/images/auth.svg'
import SelectIdentity from '../components/organisms/auth/SelectIdentity'
import TokenSent from '../components/organisms/auth/TokenSent'
import NotConnected from '../components/organisms/auth/NotConnected'
import CreateAccount from '../components/organisms/auth/CreateAccount'

export default function LoginPage() {
  const [session, loading] = useSession()

  const emailInputRef = useRef()
  const uuidInputRef = useRef()
  const nameInputRef = useRef()

  const createUser = (enteredEmail, enteredUuid, enteredName) => {
    console.log('hit')
    return axios
      .post('api/auth/tokensignup', {
        email: enteredEmail,
        uuid: enteredUuid,
        name: enteredName
      })
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

  async function submitHandler(event) {
    event.preventDefault()

    const enteredEmail = emailInputRef.current.value
    const enteredUuid = uuidInputRef.current.value
    const enteredName = nameInputRef.current.value

    try {
      const result = await createUser(enteredEmail, enteredUuid, enteredName)
      console.log(result)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <AppContainer>
      <NavBar />

      <PageContent>
        <Login.Layout>
          <Image src={authImage} alt='Authentication Image' />
          <Login.Container>
            {/* <SelectIdentity /> */}
            {/* <TokenSent /> */}
            {/* <NotConnected /> */}
            {/* <CreateAccount /> */}
          </Login.Container>
        </Login.Layout>
      </PageContent>

      <Footer />
    </AppContainer>
    // <div>
    //   <form onSubmit={submitHandler}>

    //     <div>
    //       <label htmlFor="email">Your Email</label>
    //       <input type="text" id="email" ref={emailInputRef} />
    //     </div>

    //     <div>
    //       <label htmlFor="uuid">Your Uuid</label>
    //       <input type="text" id="uuid" ref={uuidInputRef} />
    //     </div>

    //     <div>
    //       <label htmlFor="name">Your Name</label>
    //       <input type="text" id="name" ref={nameInputRef} />
    //     </div>

    //     <div>
    //       <button type="submit">Login</button>
    //     </div>

    //   </form>
    // </div>
  )
}
