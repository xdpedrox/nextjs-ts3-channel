import React from 'react';
import TsSala from './TsSala/TsSala';
import TsUser from './TsUser/TsUser';
import Aux from '../../hoc/Aux/Aux'


const channelViewer = (props) => {
  console.log(props)
  return (
    <div>
      <h2>{props.children}</h2>
      {props.channel.subChannels && props.channel.subChannels.map(channel => (
        <Aux key={channel.cid}>

          <TsSala channel={channel} />
          {channel.clients.map(client => {
            return (
              <TsUser key={`${client.uuid}${client.client_nickname}`}
                cid={props.cid}
                uuid={client.uuid}
                name={client.client_nickname}
                permission={client.permission} 
                currentUserPermission={1}
                setPermissions={props.setPermissions}
                />
            );
          })}
        </Aux>
      ))}
    </div>
  )

}

export default channelViewer;