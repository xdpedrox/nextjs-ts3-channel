import React from 'react';

import LabeledInfo from '../atoms/LabeledInfo';
import LabeledLink from '../atoms/LabeledLink';
import classes from './TeamInfo.module.css';


const teamInfo = (props) => {
    return (
        <div>
            <h2 className={classes.TeamName}>Team Info</h2>
            {/* <img className={classes.Logo} src={props.logoUrl} alt={props.children} /> */}

            <LabeledInfo name="Channel Name">{props.channel_name}</LabeledInfo>

            {/* <LabeledLink name="Channel Owner">{props.owner.name}</LabeledLink>
            <LabeledInfo name="Channel Name">{props.team.teamName}</LabeledInfo>
            <LabeledInfo name="Area"> {props.team.gameArea} </LabeledInfo>

            <LabeledInfo name="Numero do Canal">{props.team.channelOrder}</LabeledInfo>
            <LabeledInfo name="Numero de Membros">{props.team.numberOfMember}</LabeledInfo> */}

            <LabeledLink action={props.createServerGroup}>Criar Group</LabeledLink>

        </div>
    );

}

export default teamInfo;