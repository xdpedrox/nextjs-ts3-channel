import React from 'react';


import classes from './TsUser.module.css';

const ts3User = (props) => {

  const getImgUrl = () => {
    let img = "";

    switch (props.permission) {
      case 1:
        img = "https://img.icons8.com/dusk/64/000000/teamspeak-new-logo.png";    //Channel Admin
        break;
      case 2:
        img = "https://img.icons8.com/dusk/64/000000/teamspeak-new-logo.png";     //Channel Admin
        break;
      case 3:
        img = "https://img.icons8.com/ios/50/000000/teamspeak-new-logo-filled.png";        //Mod
        break;
      case 4:
        img = "https://img.icons8.com/nolan/64/000000/teamspeak.png";        //Member
        break;
      default:
        img = "";       //Guest
        break;
    }

    return img;
  }

  // const set

  return (
    <div>
      <p className={classes.TsUser}>{props.name}</p>
      <p className={classes.TsUser}>{props.uuid}</p>
      <p className={classes.TsUser}>{props.permission}</p>

      {props.permission <= 4 && (
      <img className={classes.Image} src={getImgUrl()} alt="Permission" />
      )}

      {/* Promote */}
      {(props.permission > props.currentUserPermission+1) && (
        <div className={classes.Promote} onClick={() => props.setPermissions(props.cid, props.uuid, props.permission-1)}>
          Promote
        </div>
      )}
      {/* Demote */}
      {(props.permission > props.currentUserPermission) && props.permission <= 4 && (
        <div className={classes.Demote} onClick={() => props.setPermissions(props.cid, props.uuid, props.permission+1)}>
          Demote
        </div>
      )}
    </div>
  );

}

export default ts3User;