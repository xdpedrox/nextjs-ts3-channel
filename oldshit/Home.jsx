import React from 'react';
import useUser from './hooks/useUser'

import ManageTeam from '../ManageTeam/ManageTeam'
import CreateTeam from '../CreateTeam/CreateTeam'
import NotConnected from '../NotConnected/NotConnected'

function Home() {
  const [user] = useUser('129.12.147.234')

  const renderChannel = () => {
    if (user.uuid && user.cid) {
      return (      
        <ManageTeam user={user}/>
        )
    } else if (user.uuid && !user.cid) {
      return ( 
        <CreateTeam user={user}/>
      )
    } else {
      return (
        <NotConnected />
        )
    }
  }

  return (
    <div>
      {renderChannel()}
    </div>
  );
}

export default Home;
