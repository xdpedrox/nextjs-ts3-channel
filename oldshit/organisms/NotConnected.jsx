import React, {Component} from 'react';

const NotConnected = (props) => {
  return (          
    <div className='text-center'>Please connect to the TeamSpeak Server</div>
  )
}

export default NotConnected;