import React, {Component, useEffect} from 'react';
import TeamInfo from '../../../components/TeamInfo/TeamInfo'
import ChannelViewer from '../../../components/ChannelViewer/ChannelViewer'
import axios from 'axios';
import useChannel from './useChannel'
import classes from './ManageTeam.module.css';

import LabeledInfo from '../../../components/TeamInfo/LabeledInfo';
import LabeledLink from '../../../components/TeamInfo/LabeledLink';

const ManageTeam = (props) => {
  const [subChannels, createServerGroup, setPermissions] = useChannel(props.user)

  console.log(props)
  return (
    <div>
       <h1>Channel Manage</h1>
       <h1 className='text-center'>{props.user.cid}</h1>
        <h1 className='text-center'>{props.user.channel_name}</h1>
        <div>
          <h2 className={classes.TeamName}>Team Info</h2>

          <LabeledInfo name="Channel Name">{props.user.channel_name}</LabeledInfo>

          {/* <LabeledLink name="Channel Owner">{props.owner.name}</LabeledLink>
          <LabeledInfo name="Channel Name">{props.team.teamName}</LabeledInfo>
          <LabeledInfo name="Area"> {props.team.gameArea} </LabeledInfo>
          <LabeledInfo name="Numero do Canal">{props.team.channelOrder}</LabeledInfo>
          <LabeledInfo name="Numero de Membros">{props.team.numberOfMember}</LabeledInfo> */}

          <LabeledLink action={createServerGroup}>Criar Group</LabeledLink>

        </div>
        <ChannelViewer 
          cid={props.user.cid}
          channel={subChannels}
          setPermissions={setPermissions}
          user={props.user} /> 
    </div>
    )
}

export default ManageTeam;