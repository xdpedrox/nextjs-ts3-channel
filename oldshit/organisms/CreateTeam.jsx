import React, {Component} from 'react';

import useChannel from '../../hooks/useCreateChannel'
import { useSession, signOut } from 'next-auth/client';

const CreateTeam = (props) => {
  const [session, loading] = useSession()

  const [channel, onChannelName, onChannelPassword, onMove, onSubmit] = useChannel(session?.user?.uuid || null)

  const createChannel = async () => {
    onSubmit(channel.name, channel.password, channel.move).then(e =>{
      console.log("Success")
    }).catch(e => {
      console.log(e)
    })
  }

  return (
    <div>
      <div className='text-center'>Welcome Hard</div>
      <div className='text-center'>Create TeamSpeak Channel</div>
      <input value={channel.name} onChange={onChannelName} />
      <input value={channel.password} onChange={onChannelPassword} />
      <input type='checkbox' checked={channel.move} onChange={onMove} />

      <button onClick={createChannel}>Create Channel</button>
    </div>
  );
};

export default CreateTeam;
