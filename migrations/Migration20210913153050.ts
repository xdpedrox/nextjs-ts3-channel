import { Migration } from '@mikro-orm/migrations';

export class Migration20210913153050 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table `verification_request` (`id` int unsigned not null auto_increment primary key, `identifier` varchar(255) not null, `token` varchar(255) not null, `expires` datetime not null, `created_at` datetime not null, `updated_at` datetime not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `verification_request` add unique `verification_request_token_unique`(`token`);');
    this.addSql('alter table `verification_request` add unique `verification_request_expires_unique`(`expires`);');

    this.addSql('create table `team` (`id` int unsigned not null auto_increment primary key, `name` varchar(255) not null, `cid` int(11) not null, `server_group_id` int(11) null, `last_used_at` datetime not null, `created_at` datetime not null, `updated_at` datetime not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `team` add unique `team_name_unique`(`name`);');
    this.addSql('alter table `team` add unique `team_cid_unique`(`cid`);');
    this.addSql('alter table `team` add unique `team_server_group_id_unique`(`server_group_id`);');

    this.addSql('create table `user` (`id` int unsigned not null auto_increment primary key, `uuid` varchar(255) not null, `name` varchar(255) not null, `email` varchar(255) not null, `email_verified` datetime null, `team_id` int(11) unsigned null, `team_permission` int(11) null, `created_at` datetime not null, `updated_at` datetime not null) default character set utf8mb4 engine = InnoDB;');
    this.addSql('alter table `user` add unique `user_uuid_unique`(`uuid`);');
    this.addSql('alter table `user` add index `user_team_id_index`(`team_id`);');

    this.addSql('alter table `user` add constraint `user_team_id_foreign` foreign key (`team_id`) references `team` (`id`) on update cascade on delete set null;');
  }

}
