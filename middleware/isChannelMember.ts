import { User } from '../entities/entities'

function isChannelMember (user: User & {}) {
  return new Promise((resolve, reject) => {
    if (user.isMember(user.team)) {
      return resolve(true)
    }
    return reject({ message: 'User is not channel Admin' })
  })
}

export default isChannelMember
