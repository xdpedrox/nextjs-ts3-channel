import { User } from "../entities/entities";

function hasTeam(user: User & {}) {
  return new Promise((resolve, reject) => {
    if (user.team) {
      return resolve(true);
    }
    return reject({ message: "User doesn't have a team" });
  });
}

export default hasTeam;
