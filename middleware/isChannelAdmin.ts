import { User } from '../entities/entities'

function isChannelAdmin (user: User & {}) {
  return new Promise((resolve, reject) => {
    if (user.isAdmin(user.team)) {
      return resolve(true)
    }
    return reject({ message: 'User is not channel Admin' })
  })
}

export default isChannelAdmin
