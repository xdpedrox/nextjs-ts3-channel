import { NextApiRequest, NextApiResponse } from 'next'
import { Session } from 'next-auth';

function isAuthenticated(session: Session) {
  return new Promise((resolve, reject) => {
    if (!session) {
      // code 401
      return reject({ message: 'Not authenticated!' })
    }
    return resolve({})
  })
}

export default isAuthenticated
