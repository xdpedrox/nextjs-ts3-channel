import { User } from '../entities/entities'

function isChannelMod (user: User & {}) {
  return new Promise((resolve, reject) => {
    if (user.isMod(user.team)) {
      return resolve(true)
    }
    return reject({ message: 'User is not channel Admin' })
  })
}

export default isChannelMod
