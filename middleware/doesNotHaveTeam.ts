import { User } from "../entities/entities";

function doesNotHaveTeam(user: User & {}) {
  return new Promise((resolve, reject) => {
    if (!user.team) {
      return resolve(true);
    }
    return reject({ message: "User has a team" });
  });
}

export default doesNotHaveTeam;
