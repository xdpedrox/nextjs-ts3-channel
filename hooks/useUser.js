import { useReducer, useEffect } from 'react';
import axios from 'axios';

const initialState = {
  connected: false,
  uuid: null,
  client_nickname: null,

  cid: null,
  name:  null,
  serverGroupId: null
};

const CONNECTED = 'CONNECTED'
const SET_CHANNEL = 'SET_CHANNEL'
const NOT_CONNECTED = 'NOT_CONNECTED'

function userReducer(state, action) {
  console.log(action);
  switch (action.type) {
    case CONNECTED: {
      return {
        ...state,
        uuid: action.uuid,
        client_nickname: action.client_nickname,
      };
    }
    case SET_CHANNEL: {
      return {
        ...state,
        cid: action.cid,
        name:  action.name,
        serverGroupId: action.serverGroupId,
      };
    }
    case NOT_CONNECTED: {
      return {
        ...initialState,
      };
    }

    default:
      return state;
  }
}

const useUser = (ip) => {

  const [user, dispatch] = useReducer(userReducer, initialState);

  useEffect(async () => {
    try {
      const userResponse = await axios.post('http://127.0.0.1:1337/user/clients', { ip })

      if (userResponse.data.clients.length > 0) {
        const userObject = {
          uuid: userResponse.data.clients[0].uuid,
          client_nickname: userResponse.data.clients[0].client_nickname
        }

        dispatch({ type: CONNECTED, ...userObject });


        const teamResponse = await axios.post('http://127.0.0.1:1337/team/', {
          uuid: userObject.uuid
        })

        if (!teamResponse.data.no_channel) {
          const teamObject = {
            cid: teamResponse.data.channels[0].cid,
            name: teamResponse.data.channels[0].name,
            serverGroupId: teamResponse.data.channels[0].serverGroupId
          }

          dispatch({
            type: SET_CHANNEL,
            ...teamObject
          });
        }
      } else {
        dispatch({ type: NOT_CONNECTED });
      }
    } catch (err) {
      console.log('Error checking user');
    }
  }, []);

  return [
    user,
  ];
};

export default useUser;
