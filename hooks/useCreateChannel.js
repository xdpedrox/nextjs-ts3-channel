import { useReducer } from 'react';
import axios from 'axios';

const initialState = {
  name: null,
  password: null,
  uuid: null,
  move: false,
};

const SET_CHANNEL_NAME = 'SET_CHANNEL_NAME'
const SET_CHANNEL_PASSWORD = 'SET_CHANNEL_PASSWORD'
const SET_MOVE = 'SET_MOVE'

function channelReducer(state, action) {
  switch (action.type) {
    case SET_CHANNEL_NAME: {
      return {
        ...state, name: action.name
      };
    }
    case SET_CHANNEL_PASSWORD: {
      return {
        ...state, password: action.password
      };
    }
    case SET_MOVE: {
      return {
        ...state, move: !state.move,
      };
    }
    default: {
      return state;
    }
  }
}

const useCreateChannel = (uuid) => {
  const [channel, dispatch] = useReducer(channelReducer, initialState);

  const onChannelName = (event) => {
    dispatch({ type: SET_CHANNEL_NAME, name: event.target.value });
  }

  const onChannelPassword = (event) => {
    dispatch({ type: SET_CHANNEL_PASSWORD, password: event.target.value });
  }

  const onMove = () => {
    dispatch({ type: SET_MOVE });
  }

  const createTeam = async (channel_name, channel_password, move) => {
    try {
      const response = await axios.put('http://127.0.0.1:1337/team', {
        channel_name, channel_password, uuid, move
      })

      if (response.data.uuid) {
        dispatch({ type: 'connected', uuid: uuid, client_nickname: response?.data?.client_nickname });
      } else {
        dispatch({ type: 'notconnected' });
      }
    } catch (err) {
      console.log('Error checking user');
    }
  };

  return [
    channel,
    onChannelName,
    onChannelPassword,
    onMove,
    createTeam,
  ];
};

export default useCreateChannel;
