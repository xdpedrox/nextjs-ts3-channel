import { User, Team, VerificationRequest } from '../entities/entities'
import { Options } from "@mikro-orm/core";
import dotenv from "dotenv";
dotenv.config();

const config: Options = {
  dbName: process.env.DB_NAME,
  type: "mariadb",
  host: process.env.DB_HOST,
  port: 3306,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  entities: [User, Team, VerificationRequest],
  debug: process.env.NODE_ENV === "development",
};

export default config;
