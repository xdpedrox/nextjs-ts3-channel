import "reflect-metadata";
import { EntityManager, EntityRepository, MikroORM, RequestContext } from '@mikro-orm/core';
import { User, Team, VerificationRequest } from '../entities/entities'
import config from "./mikro-orm";

interface dbAttributes {
  orm: MikroORM;
  em: EntityManager;
  teamRepo: EntityRepository<Team>;
  userRepo: EntityRepository<User>;
  verificationRequestRepo: EntityRepository<VerificationRequest>;
}

const db = async (): Promise<dbAttributes>  => {
  const orm = await MikroORM.init(config)
  return {
    orm,
    em: orm.em,
    teamRepo: orm.em.getRepository(Team),
    userRepo: orm.em.getRepository(User),
    verificationRequestRepo: orm.em.getRepository(VerificationRequest),
  }
}

export default db;
