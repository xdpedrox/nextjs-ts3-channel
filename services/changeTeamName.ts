interface ResponseType {
  statusCode: number;
  body: any;
}

const response = (statusCode: number, body: object) => {
  return {
    statusCode,
    body,
  };
};

import {
  teamCreate,
  teamDestroy,
  teamChangeName,
  teamCreateServerGroup,
  teamResetPermissions,
} from "../lib/ts3/src/index";
import getDb from "../config/initialize-database";


const changeTeamName = async (
  team: any,
  team_name: string,
): Promise<ResponseType> => {
  try {
    const db = await getDb()

    if (team.name == team_name)
      return response(406, { message: "Can't change name to same name" });

    if ((await db.teamRepo.count({ name: team_name })) != 0)
      return response(406, { message: "Name already in use in db" });

    await teamChangeName(team.cid, team_name, team.serverGroupId);

    team.name = team_name

    db.teamRepo.persist(team).flush()

    return response(200, team.toJSON());
  } catch (err) {
    console.log(err)
    return response(500, { message: "Failed to change Name" });
  }
};

export default changeTeamName;
