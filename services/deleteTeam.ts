interface ResponseType {
  statusCode: number;
  body: any;
}

const response = (statusCode: number, body: object) => {
  return {
    statusCode,
    body,
  };
};
import { Team } from '../entities/entities'
import {
  teamDestroy,
} from "../lib/ts3/src/index";
import getDb from "../config/initialize-database";


const deleteTeam = async (
  team: Team,
) => {
  try {
    const db = await getDb()
    await teamDestroy(team.cid.toString(), team.serverGroupId?.toString() || null)
    // Delete team from users
    const users = await team.users.getItems().map(user => {
      user.teamPermission = null
      user.team = null
      return user
    })
    await db.userRepo.persistAndFlush(users)
    await db.teamRepo.removeAndFlush(team)

  } catch (err) {
    throw { message: "Failed to change Name" };
  }
};

export default deleteTeam;
