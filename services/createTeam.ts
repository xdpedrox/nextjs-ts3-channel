import {errors} from '../lib/ts3/src/responses/BaseError'

interface ResponseType {
  statusCode: number;
  body: any;
}

const response = (statusCode: number, body: object) => {
  return {
    statusCode,
    body,
  };
};

import {
  teamCreate,
} from "../lib/ts3/src/index";
import getDb from "../config/initialize-database";


const createTeam = async (
  uuid: string,
  team_name: string,
  password: string,
  move: boolean
): Promise<ResponseType> => {
  try {
    const db = await getDb()
    const user = await db.userRepo.findOne({ uuid: uuid }, ['team']);

    if (!user) 
      return response(500, { message: "User not found" });
      
    // Does user have a team
    if (user?.team)
      return response(406, { message: "User already has a team" });

    // Checking if team with the same name exists in the dB
    if (await db.teamRepo.findOne({name: team_name}))
      return response(406, { message: "Name already in use in use" });

    // Creating channel on teamspeak
    const createResult = await teamCreate(team_name, password, uuid, move);

    if (createResult.mainChannelId) {
      const team = await db.teamRepo.create({
        cid: createResult.mainChannelId,
        name: team_name,
      });

      user.teamPermission = 1
      user.team = team

      await db.em.persist([user, team]).flush()
    
      return response(200, {
        message: "Channel Created",
        data: team.toOBjectWithUsers(),
      });
    } else {
      return response(500, { message: "Error occured creating channel"});
    }
  } catch (err) {
    console.log(err)

    if (err.name == errors.CHANNEL_NAME_ALREADY_IN_USE) {
      // TODO add reverse action
      return response(406, { message: "Channel Name in use in TeamSpeak" });
    } else {
      return response(500, { message: "Error occured creating channel" });
    }
  }
};

export default createTeam;
