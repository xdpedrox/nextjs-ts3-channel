interface ResponseType {
  statusCode: number
  body: any
}

const response = (statusCode: number, body: object) => {
  return {
    statusCode,
    body
  }
}
import { Team, User } from '../entities/entities'
import { teamSetPermission } from '../lib/ts3/src/index'
import getDb from '../config/initialize-database'

const setUser = async (
  team: Team,
  user: User,
  permission: 1 | 2 | 3 | 4 | 5
): Promise<ResponseType> => {
  try {
    const db = await getDb()

    if (user.team  && permission < 5)
      return response(406, { message: 'User already has a team' })
    
    // TODO: Copy
    // if (
    //   // can give 2 admin, 3 mod, 4 member, remove
    //   (user.isAdmin(team) && permission > 1) ||
    //   // can give 4 member, remove
    //   (user.isMod(team) && permission > 3)
    // ) {
    //   throw response(406, { message: "You don't have enough powa" })
    // }

    await teamSetPermission(
      team.cid.toString(),
      user.uuid,
      permission,
      team.serverGroupId?.toString()
    )

    if (permission < 5) {
      // Add user
      user.team = team
      user.teamPermission = permission
      db.userRepo.persist(user).flush()
  
      return response(200, response(201, { message: 'User added' }))
    } else {
      // Remove User
      user.team = null
      user.teamPermission = null
      db.userRepo.persist(user).flush()
  
      return response(200, response(201, { message: 'User Removed' }))
    }
  } catch (err) {
    console.log(err)
    return response(500, { message: 'Failed to add user' })
  }
}

export default setUser
