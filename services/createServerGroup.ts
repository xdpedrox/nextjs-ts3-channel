interface ResponseType {
  statusCode: number;
  body: any;
}

const response = (statusCode: number, body: object) => {
  return {
    statusCode,
    body,
  };
};
import { Team } from '../entities/entities'
import {
  teamCreate,
  teamDestroy,
  teamChangeName,
  teamCreateServerGroup,
  teamResetPermissions,
} from "../lib/ts3/src/index";
import getDb from "../config/initialize-database";


const createServerGroup = async (
  team: Team,
): Promise<void> => {
  try {
    const db = await getDb()

    if (team.serverGroupId)
      throw { message: "Team already have a ServerGroup" };

    const uuids = (await team.users.loadItems()).map(user => user.uuid)
    const serverGroup = await teamCreateServerGroup(team.name, uuids)

    team.serverGroupId = +serverGroup.sgid

    await db.teamRepo.persist(team).flush()

  } catch (err) {
    throw response(500, { message: "Failed to change Name" });
  }
};

export default createServerGroup;
