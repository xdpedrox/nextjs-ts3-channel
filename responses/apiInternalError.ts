import BaseError from "./BaseError";
import { StatusCode } from 'status-code-enum'


class ApiInternalError extends BaseError {
  constructor(name, httpCode = StatusCode.ServerErrorInternal, message = 'Internal Server Error', isOperational = true) {
    super(name, httpCode, message, isOperational);
  }
 }

 export default ApiInternalError