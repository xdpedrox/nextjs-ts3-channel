import ApiInternalError from './apiInternalError'
import { StatusCode } from 'status-code-enum'


class ApiForbidden extends ApiInternalError {
  constructor(name, httpCode = StatusCode.ClientErrorForbidden, message = 'Forbidden', isOperational = true) {
    super(name, httpCode, message, isOperational);
  }
 }

 export default ApiForbidden