import ApiInternalError from './apiInternalError'
import { StatusCode } from 'status-code-enum'


class ApiBadRequest extends ApiInternalError {
  constructor(name, httpCode = StatusCode.ClientErrorBadRequest, message = 'Bad Request', isOperational = true) {
    super(name, httpCode, message, isOperational);
  }
 }

 export default ApiBadRequest