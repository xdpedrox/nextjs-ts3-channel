import { StatusCode } from 'status-code-enum'

class BaseError extends Error {
  public readonly name: string;
  public readonly httpCode: StatusCode;
  public readonly isOperational: boolean;
  
  constructor(name: string, httpCode: StatusCode, message: string, isOperational: boolean) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  
    this.name = name;
    this.httpCode = httpCode;
    this.isOperational = isOperational;
  
    Error.captureStackTrace(this);
  }
 }

 export default BaseError