import ApiInternalError from './BaseError'
import { StatusCode } from 'status-code-enum'


class ApiUnauthorized extends ApiInternalError {
  constructor(name, httpCode = StatusCode.ClientErrorUnauthorized, message = 'Unauthorized', isOperational = true) {
    super(name, httpCode, message, isOperational);
  }
 }

 export default ApiUnauthorized