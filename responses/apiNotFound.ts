import ApiInternalError from './apiInternalError'
import { StatusCode } from 'status-code-enum'


class ApiNotFound extends ApiInternalError {
  constructor(name, httpCode = StatusCode.ClientErrorNotFound, message = 'Not found', isOperational = true) {
    super(name, httpCode, message, isOperational);
  }
 }

 export default ApiNotFound