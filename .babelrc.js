const dev = process.env.NODE_ENV !== 'production';


module.exports = {
  "presets": ["next/babel"],
  "plugins": [
    "babel-plugin-transform-typescript-metadata",
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
    ["@babel/plugin-proposal-class-properties" ],
    ['styled-components', { ssr: !dev, displayName: dev }],
  ]
}