import getDb from "./config/initialize-database";
import createTeam from "./services/createTeam";
import { wrap } from '@mikro-orm/core';
import changeTeamName from "./services/changeTeamName";
import createServerGroup from "./services/createServerGroup";
import deleteTeam from "./services/deleteTeam";
import setUser from "./services/setUser";

const test = async () => {
 const db = await getDb()

 const user = await db.userRepo.findOne({uuid: "smgDpLgFrZmV2FrK/PwWvFCA2Pw="})

 const team = await db.teamRepo.findOne({id: 3})
 const team2 = await db.teamRepo.findOne({id: 4})

//  const team = await db.teamUserRepo.findOne({id: 1}, ['user', 'team'])


//  const team = await db.teamRepo.create({name: "testt", cid: 638})
// // const result = await db.teamRepo.persist(team).flush();

//  const teamUser = await db.teamUserRepo.create({permission: 1})
//  teamUser.user = user
//  teamUser.team = team
user.team = team2
user.teamPermission = 2
await db.userRepo.persistAndFlush(user)
// //  add(user)
console.log(user)
console.log(user.team)
console.log(user.isAdmin(team))
console.log(user.isMod(team))
console.log(user.isMember(team))

console.log(team.isMember(user))
console.log(team.isMod(user))
console.log(team.isAdmin(user))


// console.log(user.teamUsers[0].team)
//  const result = await db.teamUserRepo.persist(teamUser).flush();

//  console.log(await user.teamUsers.loadItems())
}

// test()

const uuid1 = "smgDpLgFrZmV2FrK/PwWvFCA2Pw="
const uuid2 = "C1gYmYrnaxThvffXHCWOBA60K0Y="

const teamSpeakTest = async () => {
  const db = await getDb()

  const team = await db.teamRepo.findOne({id: 4}, )

  const user1 = await db.userRepo.findOne({uuid: uuid1})
  const user2 = await db.userRepo.findOne({uuid: uuid2})

  const result = await setUser(team, user2, 5)
  
  console.log(result)
}

teamSpeakTest()

export {}