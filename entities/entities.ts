import {
  PrimaryKey,
  Entity,
  Property,  
  Unique,
  OneToMany,
  ManyToOne,
  Collection,
  wrap
} from "@mikro-orm/core";

@Entity()
export class Team {

  @PrimaryKey()
  id!: number;

  @Property()
  @Unique()
  name!: string;

  @Property()
  @Unique()
  cid!: number;

  @Property({ nullable: true })
  @Unique()
  serverGroupId?: number;

  @OneToMany(() => User, user => user.team)  
  users = new Collection<User>(this);

  @Property()
  lastUsedAt: Date = new Date();

  @Property()
  createdAt: Date = new Date();

  @Property({
    type: 'Date',
    onUpdate: () => new Date(),
  })
  updatedAt: Date = new Date();

  toOBjectWithUsers () {
    const teamObject = wrap(this).toObject()
    return {
      ...teamObject,
      users: this.users.getItems().map(user => {
        return wrap(user).toObject()
      })
    }
  }

  isAdmin(user: User) {
    // this.users.loadItems()
    return this.id === user.team?.id && user.teamPermission <= 2
  }

  isMod(user: User) {
    // this.users.loadItems()
    return this.id === user.team?.id && user.teamPermission <= 3
  }

  isMember(user: User) {
    // this.users.loadItems()
    return this.id === user.team?.id && user.teamPermission <= 4
  }

}

@Entity()
export class User {

  @PrimaryKey()
  id!: number;

  @Property()
  @Unique()
  uuid!: string;

  @Property()
  name!: string;

  @Property()
  email!: string;

  @Property({ nullable: true })
  emailVerified?: Date;

  @ManyToOne(() => Team, { nullable: true }) 
  team!: Team;

  @Property({ nullable: true })
  teamPermission!: number;

  @Property()
  createdAt: Date = new Date();

  @Property({
    type: 'Date',
    onUpdate: () => new Date(),
  })
  updatedAt: Date = new Date();

  isAdmin(team: Team) {
    return this.team?.id === team?.id && this.teamPermission <= 2
  }

  isMod(team: Team) {
    return this.team?.id === team?.id && this.teamPermission <= 3
  }

  isMember(team: Team) {
    return this.team?.id === team?.id && this.teamPermission <= 4
  }
}

@Entity()
export class VerificationRequest {

  @PrimaryKey()
  id!: number;

  @Property()
  identifier!: string;

  @Property()
  @Unique()
  token!: string;

  @Property()
  @Unique()
  expires?: Date;

  @Property()
  createdAt: Date = new Date();

  @Property({
    type: 'Date',
    onUpdate: () => new Date(),
  })
  updatedAt: Date = new Date();
}
